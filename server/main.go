package main

import (
    "github.com/gin-gonic/gin"
    "log"
    "gitlab.com/tsuchina.ga/waaw/business-card/server/service"
    "gopkg.in/mgo.v2/bson"
    "net/http"
    "io/ioutil"
    "gitlab.com/tsuchina.ga/waaw/business-card/server/entity"
    "encoding/json"
)

func main() {
    log.Println("hello easy-chat")

    r := gin.Default()

    r.NoRoute(func(c *gin.Context) {
        c.JSON(http.StatusNotFound, gin.H{"code": "PAGE_NOT_FOUND", "message": "Page not found"})
    })

    r.GET("/:name", func(c *gin.Context) {
        name, _ := c.Params.Get("name")

        card, err := service.FindByName(name)
        if err != nil {
            c.JSON(http.StatusNotFound, gin.H{"code": "PAGE_NOT_FOUND", "message": "Page not found"})
        } else {
            jsonCard, _ := bson.MarshalJSON(card)
            c.Data(http.StatusOK, "application/json", jsonCard)
        }
    })

    r.POST("/", func(c *gin.Context) {
        body, _ := ioutil.ReadAll(c.Request.Body)
        card := entity.Card{}
        if err := json.Unmarshal(body, &card); err != nil {
            log.Println(err)
            c.String(http.StatusBadRequest, "400 bad request")
        } else {
            card, err := service.Create(card)
            if err != nil {
                log.Print("create failed")
                c.String(http.StatusBadRequest, "400 bad request")
            } else {
                jsonCard, _ := bson.MarshalJSON(card)
                c.Data(http.StatusOK, "application/json", jsonCard)
            }
        }
    })

    r.Run(":18081")
}
