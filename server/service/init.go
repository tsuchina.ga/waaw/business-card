package service

import (
    "gopkg.in/mgo.v2"
    "os"
    "log"
)

var db *mgo.Database

func init() {
    session, err := mgo.DialWithInfo(&mgo.DialInfo{
        Addrs: []string{ "bizcarddb", },
        Database: "bizcard",
        Username: "bizcard",
        Password: "bizcard",
    })
    if err != nil {
        log.Println(err)
        os.Exit(-1)
    }
    db = session.DB("bizcard")
}
