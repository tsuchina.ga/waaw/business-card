package service

import (
    "gitlab.com/tsuchina.ga/waaw/business-card/server/entity"
    "gopkg.in/mgo.v2/bson"
)

func FindByName(name string) (entity.Card, error) {
    card := entity.Card{}
    err := db.C("card").Find(bson.M{"name": name}).One(&card)
    return card, err
}

func Create(card entity.Card) (entity.Card, error) {
    err := db.C("card").Insert(card)
    return card, err
}
