package entity

import "gopkg.in/mgo.v2/bson"

type Card struct {
    Id   bson.ObjectId     `json:"-" bson:"-"`
    Name string            `json:"name" bson:"name"`
    Web  map[string]string `json:"web" bson:"web"`
}
