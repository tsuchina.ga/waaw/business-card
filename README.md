# _WAAW02_ business-card

毎週1つWebアプリを作ろうの2回目。

期間は18/05/18 - 18/05/24

てか速攻Webアプリちゃうやん。

## ゴール

* [ ] `go run xxx`みたいなgoさえ入っていればすぐに見れる形
* [ ] コマンドラインで見られる


## 目的

* [x] CLI名刺という流行に乗る
* [x] GolangでCLIツールを作ってみる


## 課題

* [x] windows, max, linux問わず動く

    `github.com/pkg/browser`がブラウザでのオープンをカバーしてくれる。

* [ ] 画像が表示される
* [x] キー入力は矢印のみで操作できる

    `github.com/AlecAivazis/survey`がselect操作を簡単に実装してくれる

* [x] mongoDBを利用するserverを作って、そこに名刺データをためる

* [ ] clientはそのサーバにアクセスして、特定の名刺をみたり追加したりする

* [ ] clientは`go get`なり`go install`なりして取得し、goコマンドを使わずに実行できる


## 参考

* [AlecAivazis/survey: A golang library for building interactive prompts with full support for windows and posix terminals.](https://github.com/AlecAivazis/survey)
* [pkg/browser: Package browser provides helpers to open files, readers, and urls in a browser window.](https://github.com/pkg/browser)
* [Go言語でMongoDB使う ①インストール的なところ - Qiita](https://qiita.com/otiai10/items/2c94bb53bf55266130c2)
* [mgo - GoDoc](https://godoc.org/labix.org/v2/mgo#DialWithInfo)
* [mongodb - Mgo omit field even when not empty - Stack Overflow](https://stackoverflow.com/questions/22846879/mgo-omit-field-even-when-not-empty)
* [go - Setting up Route Not Found in Gin - Stack Overflow](https://stackoverflow.com/questions/32443738/setting-up-route-not-found-in-gin)
* [gin-gonic/gin: Gin is a HTTP web framework written in Go (Golang). It features a Martini-like API with much better performance -- up to 40 times faster. If you need smashing performance, get yourself some Gin.](https://github.com/gin-gonic/gin#model-binding-and-validation)


## 振り返り

0. Idea:

    アイデアやテーマについて
    * Qiitaに影響されてはじめただけで、アイデアは良くも悪くもない
    * テーマとしてcliを作るという意味ではよかった
    * 結局既存のプラグインを組み合わせるだけで構成のなものが実装できることがわかった


0. What went right:

    成功したこと・できたこと
    * CLIで必要な読み書きの技術
    * mongoDBの環境構築
    * mongoDBへのクエリ発行


0. What went wrong:

    失敗したこと・できなかったこと
    * 通信しながら表示を変えるCLIの作成
    * CRUDなWebAPI
    * 複雑なデータの保持(mongoDBの得意とするところを使えなかった


0. What I learned:

    学べたこと
    * nosqlの代表格、mongoDBでできること・できないこと
    * CLIが得意なこと・苦手なこと
    * 本業・副業が忙しいと毎週1つ挑戦するのはかなり厳しいということ
    * たとえばブラウザのオープンはOSごとに違い、それをgolang側で判断して切り分けなくてはいけない
    * サーバをクリーンアクテクチャで実装するには多くの知識を要し、今の状態ではまだ足りない


## サンプル

このリポジトリのclientが唯一の成果物？
