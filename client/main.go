package main

import (
    "gopkg.in/AlecAivazis/survey.v1"
    "github.com/pkg/browser"
    "fmt"
)

func main() {
    fmt.Println("こんにちは、つちなが(@tsuchinaga)です。")

    prompt := &survey.Select{
        Message: "Choose an action",
        Options: []string{"Gitlab", "Twitter", "Exit"},
    }

    for {
        action := ""
        survey.AskOne(prompt, &action, nil)

        switch action {
        case "Gitlab": browser.OpenURL("https://gitlab.com/tsuchinaga")
        case "Twitter": browser.OpenURL("https://twitter.com/tsuchinaga")
        default:
            fmt.Println("Thank you.")
            return
        }
    }
}
